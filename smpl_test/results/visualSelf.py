'''
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
'''
'''
    Loading the log data for each expriment and visualize
    Currently four lines: MRA WA-AD WA-AR RRT-Connect RRT* (in this order)
    plotting each case differently: 0 --> together
                                    1 --> scenerio 1
                                    2 --> scenerio 2
                                    3 --> scenerio 3
                                    4 --> scenerio 4
'''

import sys
import copy
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

argList = sys.argv

numFile = len(argList) - 1
scene_idx = int(argList[numFile])

''' parsing inputs'''
fMRA = open(argList[1],'r')


data = []
# for fFILE in {fMRA}:
    # while True:
        # line = fFILE.readline().strip()
        # if not line:
            # break
        # # Expansion Time Post_time Cost Post_cost Succ_flag 
        # meta = [0,0,0,0,0,0]
        
        # # post cost
        # meta[4] = line.split(' ')[1]
        # meta[4] = float(meta[4])/1000.00

        # # post time
        # line = fFILE.readline().strip()
        # meta[2] = line.split(' ')[1]
        # meta[2] = float(meta[2])

        # # time
        # line = fFILE.readline().strip()
        # meta[1] = line.split(' ')[1]
        # meta[1] = float(meta[1])

        # # EXpansion
        # line = fFILE.readline()
        # meta[0] = line.split(' ')[1]
        # meta[0] = float(meta[0])

        # # cost
        # line = fFILE.readline()
        # meta[3] = line.split(' ')[1]
        # meta[3] = float(meta[3])/1000.00

        # # succ flag
        # line = fFILE.readline()
        # meta[5] = line.split(' ')[1]
        # meta[5] = int(meta[5])

        # line = fFILE.readline()

        # data.append(copy.deepcopy(meta))

for fFILE in {fMRA}:
    while True:
        line = fFILE.readline().strip()
        if not line:
            break
        # Expansion Time Post_time Cost Post_cost Succ_flag 
        meta = [0,0,0,0,0,0]
        
        # time
        meta[1] = line.split(' ')[1]
        meta[1] = float(meta[1])

        # post time
        line = fFILE.readline().strip()
        meta[2] = line.split(' ')[1]
        meta[2] = float(meta[2])

        # Expansion
        line = fFILE.readline()
        meta[0] = line.split(' ')[1]
        meta[0] = float(meta[0])

        # cost
        line = fFILE.readline()
        meta[3] = line.split(' ')[1]
        meta[3] = float(meta[3])/18000.0*3.1415926

        # post cost
        line = fFILE.readline()
        meta[4] = line.split(' ')[1]
        meta[4] = float(meta[4])/18000.0*3.1415926

        # succ flag
        line = fFILE.readline()
        meta[5] = line.split(' ')[1]
        meta[5] = int(meta[5])

        line = fFILE.readline()
        data.append(copy.deepcopy(meta))

fMRA.close()
''' Transform data '''
# Expansion Time Post_time Cost Post_cost Succ_flag 
data = np.asarray(data,dtype=np.float)
data = data.transpose()

expd = data[0]
time = data[1]
post_time = data[2]
cost = data[3]
post_cost = data[4]
succ = data[5]

if scene_idx == 1:
    time = time[0:69]
    expd = expd[0:69]
    succ = succ[0:69]
    cost = cost[0:69]
    post_time = post_time[0:69]
    post_cost = post_cost[0:69]
    figname1 = "rlt_countertop.eps"
    figname2 = "rlt_countertop.png"

elif scene_idx == 2:
    time = time[70:141]
    expd = expd[70:141]
    cost = cost[70:141]
    succ = succ[70:141]
    post_time = post_time[70:141]
    post_cost = post_cost[70:141]
    figname1 = "rlt_bookshelf.eps"
    figname2 = "rlt_bookshelf.png"

elif scene_idx == 3:
    time = time[142:211]
    expd = expd[142:211]
    cost = cost[142:211]
    succ = succ[142:211]
    post_time = post_time[142:211]
    post_cost = post_cost[142:211]
    figname1 = "rlt_industrial.eps"
    figname2 = "rlt_industrial.png"

elif scene_idx == 4:
    time = time[212:283]
    expd = expd[212:283]
    cost = cost[212:283]
    succ = succ[212:283]
    post_time = post_time[212:283]
    post_cost = post_cost[212:283]
    figname1 = "rlt_tunnel.eps"
    figname2 = "rlt_tunnel.png"




''' clear up invalid '''
idx = []
lth = len(time)
for i in range(lth):
    if expd[i] == 0 and (i not in idx):
        idx.append(i)
cost = np.delete(cost, idx, 0)
time = np.delete(time, idx, 0)
expd = np.delete(expd, idx, 0)
succ = np.delete(succ, idx, 0)
post_time = np.delete(post_time, idx, 0)
post_cost = np.delete(post_cost, idx, 0)

# time = post_time + time
succr = np.sum(succ)/len(succ)
print(succr)
''' only keep succ '''
idx = []
lth = len(time)
for i in range(lth):
    # for j in range(numFile-1):
    if succ[i] == 0 and (i not in idx):
        idx.append(i)

cost = np.delete(cost, idx, 0)
time = np.delete(time, idx, 0)
expd = np.delete(expd, idx, 0)
post_time = np.delete(post_time, idx, 0)
post_cost = np.delete(post_cost, idx, 0)

''' success rate '''
# succrate = np.sum(succ,0)
# succrate = succrate/lth
time = time + post_time
print(np.average(time))
# print(np.average(expd))
# print(np.average(post_time))
print(np.average(cost))
print(np.average(post_cost))
