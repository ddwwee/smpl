'''
 * New BSD License
 * ---------------
 *
 * Copyright © 2019 Wei Du (ddwwzzyy@gmail.com) All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * * Neither the name of Wei Du nor the names of its contributors may be used to
 *   endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
'''
'''
    Loading the log data for each expriment and visualize
    Currently four lines: MRA WA-AD WA-AR RRT-Connect RRT* (in this order)
    plotting each case differently: 0 --> together
                                    1 --> scenerio 1
                                    2 --> scenerio 2
                                    3 --> scenerio 3
                                    4 --> scenerio 4
'''

import sys
import copy
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

argList = sys.argv
numFile = len(argList) - 1

if numFile < 4:
    print("You missed some log files...")
    exit()
 

''' parsing inputs'''
fMRA = open(argList[1],'r')
fWAAD = open(argList[2],'r')
fWAAR = open(argList[3],'r')
fRRTC = open(argList[4],'r')
fRRTStar = open(argList[5], 'r')


scene_idx = int(argList[numFile])

fs = [fMRA, fWAAD, fWAAR]
fr = [fRRTC, fRRTStar]

data = []
for fFILE in fs:
    eachData = []
    while True:
        line = fFILE.readline().strip()
        if not line:
            break
        # Expansion Time Post_time Cost Post_cost Succ_flag 
        meta = [0,0,0,0,0,0]
        
        # post cost
        meta[4] = line.split(' ')[1]
        meta[4] = float(meta[4])/1000.00

        # post time
        line = fFILE.readline().strip()
        meta[2] = line.split(' ')[1]
        meta[2] = float(meta[2])

        # time
        line = fFILE.readline().strip()
        meta[1] = line.split(' ')[1]
        meta[1] = float(meta[1])

        # EXpansion
        line = fFILE.readline()
        meta[0] = line.split(' ')[1]
        meta[0] = float(meta[0])

        # cost
        line = fFILE.readline()
        meta[3] = line.split(' ')[1]
        meta[3] = float(meta[3])/1000.00

        # succ flag
        line = fFILE.readline()
        meta[5] = line.split(' ')[1]
        meta[5] = int(meta[5])

        line = fFILE.readline()

        eachData.append(copy.deepcopy(meta))
    data.append(copy.deepcopy(eachData))

for fFILE in fr:
    eachData = []
    while True:
        line = fFILE.readline().strip()
        if not line:
            break
        # Expansion Time Post_time Cost Post_cost Succ_flag 
        meta = [0,0,0,0,0,0]
        
        # time
        meta[1] = line.split(' ')[1]
        meta[1] = float(meta[1])

        # post time
        line = fFILE.readline().strip()
        meta[2] = line.split(' ')[1]
        meta[2] = float(meta[2])

        # Expansion
        line = fFILE.readline()
        meta[0] = line.split(' ')[1]
        meta[0] = float(meta[0])

        # cost
        line = fFILE.readline()
        meta[3] = line.split(' ')[1]
        meta[3] = float(meta[3])/18000.0*3.1415926

        # post cost
        line = fFILE.readline()
        meta[4] = line.split(' ')[1]
        meta[4] = float(meta[4])/18000.0*3.1415926

        # succ flag
        line = fFILE.readline()
        meta[5] = line.split(' ')[1]
        meta[5] = int(meta[5])

        line = fFILE.readline()

        eachData.append(copy.deepcopy(meta))
    data.append(copy.deepcopy(eachData))


fMRA.close()
fWAAD.close()
fWAAR.close()
fRRTC.close()
fRRTStar.close()

''' Transform data '''
# Expansion Time Post_time Cost Post_cost Succ_flag 
data = np.asarray(data,dtype=np.float)
(a,b,c) = data.shape
expd = data[:, 0:b, 0]
expd = expd.transpose()

time = data[:, 0:b, 1]
time = time.transpose()

post_time = data[:, 0:b, 2]
post_time = post_time.transpose()

cost = data[:, 0:b, 3]
cost = cost.transpose()

post_cost = data[:, 0:b, 4]
post_cost = post_cost.transpose()

succ = data[:, 0:b, 5]
succ = succ.transpose()

figname1 = "rlt_all.eps"
figname2 = "rlt_all.png"
if scene_idx == 1:
    time = time[0:69,:]
    expd = expd[0:69,:]
    succ = succ[0:69,:]
    post_time = post_time[0:69,:]
    post_cost = post_cost[0:69,:]
    figname1 = "rlt_countertop.eps"
    figname2 = "rlt_countertop.png"

elif scene_idx == 2:
    time = time[70:141,:]
    expd = expd[70:141,:]
    cost = cost[70:141,:]
    succ = succ[70:141,:]
    post_time = post_time[70:141,:]
    post_cost = post_cost[70:141,:]
    figname1 = "rlt_bookshelf.eps"
    figname2 = "rlt_bookshelf.png"

elif scene_idx == 3:
    time = time[142:211,:]
    expd = expd[142:211,:]
    cost = cost[142:211,:]
    succ = succ[142:211,:]
    post_time = post_time[142:211,:]
    post_cost = post_cost[142:211,:]
    figname1 = "rlt_industrial.eps"
    figname2 = "rlt_industrial.png"

elif scene_idx == 4:
    time = time[212:283,:]
    expd = expd[212:283,:]
    cost = cost[212:283,:]
    succ = succ[212:283,:]
    post_time = post_time[212:283,:]
    post_cost = post_cost[212:283,:]
    figname1 = "rlt_tunnel.eps"
    figname2 = "rlt_tunnel.png"


''' clear up invalid '''
idx = []
lth = len(time)
for i in range(lth):
    for j in range(numFile-1):
        if expd[i,j] == 0 and (i not in idx):
            idx.append(i)
cost = np.delete(cost, idx, 0)
time = np.delete(time, idx, 0)
expd = np.delete(expd, idx, 0)
succ = np.delete(succ, idx, 0)
post_time = np.delete(post_time, idx, 0)
post_cost = np.delete(post_cost, idx, 0)

# time = post_time + time
''' only keep succ '''
costcp = [0,0,0,0]
timecp = [0,0,0,0]
expcp = [0,0,0,0]
succcp = [0,0,0,0]
for cp in range(4):
    ct = copy.deepcopy(cost)
    tm = copy.deepcopy(time)
    sc = copy.deepcopy(succ)
    ep = copy.deepcopy(expd) 
    idx = []
    lth = len(time)
    for i in range(lth):
        # for j in range(numFile-1):
        for j in {0,cp+1}:
            if succ[i,j] == 0 and (i not in idx):
                idx.append(i)
    ct = np.delete(ct, idx, 0)
    tm = np.delete(tm, idx, 0)
    ep = np.delete(ep, idx, 0)
    # print(ct[:,cp])
    # print(np.average(ct[:,cp]))
    # print(np.average(ct[:,0]))
    # break
    # post_time = np.delete(post_time, idx, 0)
    # post_cost = np.delete(post_cost, idx, 0)
    costcp[cp] = np.average(ct[:,cp+1])/ np.average(ct[:,0])
    timecp[cp] = np.average(tm[:,cp+1])/ np.average(tm[:,0])
    expcp[cp] = np.average(ep[:,cp+1])/ np.average(ep[:,0])

''' success rate '''
succrate = np.sum(succ,0)
succrate = succrate/lth

N = 4
# men_means = (20, 35, 30, 35, 27)
# women_means = (25, 32, 34, 20, 25)
expcp[3] = 0
expcp[2] = 0

ind = np.arange(N) 
width = 0.7       
fig = plt.figure(figsize=(5.5,5))
ax = fig.add_subplot(1, 1, 1)
plt.bar(ind, costcp , width/3, label='cost', color='lightcoral')
plt.bar(ind + width/3, timecp, width/3, label='time', color='cornflowerblue')
plt.bar(ind + width*2/3, expcp, width/3, label='Expansion', color='yellowgreen')

plt.ylabel('Improvements (X)',fontsize=15)
font = { 'size'   : 15}
matplotlib.rc('font', **font)

axes = fig.axes[0]
axes.text(0.33, 9, '10.8  10.8', horizontalalignment='center', fontsize=13);
axes.text(1.33, 6, '28.1  61.4', horizontalalignment='center', fontsize=13);
plt.xticks(ind + width/2.7 , ('WA-AD', 'WA-MR', 'RRT-C', 'RRT*'), fontsize=15)
plt.yticks(fontsize=15)
plt.legend(loc='best')
plt.plot([-0.5, 3.5], [1.0,1.0], "k")
plt.grid(axis='y',alpha=0.7)
major_ticks = np.arange(0, 12, 1.0)
ax.set_yticks(major_ticks)
ax.set_ylim([0,10])
plt.savefig("manip_cmp.pdf",bbox_inches = 'tight', dpi=500)
plt.show()

print(costcp)
print(timecp)
print(succrate)
print(expcp)

# cost = post_cost;
# time = time+post_time

''' plot the results '''

# labels = ["MRA", "WAAD", "WAAR", "RRTC", "RRT*" ]
# fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(9, 9))
# fig.tight_layout( pad=4.0, w_pad=4.0, h_pad=3.0)

# axes[0,0].boxplot(time,labels = labels,patch_artist=True,notch=False)
# axes[0,0].set_ylabel('Planning Time (s)')
# axes[0,0].set_ylim([0,117])

# expd = expd/1000
# axes[0,1].boxplot(expd,labels = labels,patch_artist=True,notch=False)
# axes[0,1].set_ylabel('Number of Expansions (1e3)')
# axes[0,1].set_ylim([0,360])

# cost = cost/1000
# axes[1,0].boxplot(post_cost,labels = labels,patch_artist=True,notch=False)
# axes[1,0].set_ylabel('Solution Cost (1e3)')

# axes[1,1].bar(labels,succrate)
# axes[1,1].set_ylabel('Success Rate (%)')

# for row in axes:
    # for column in row:
        # column.yaxis.grid(True)

''' add time comparison '''
# std = np.average(time[:,0])
# rrtt = np.average(time[:,4])
# time1 = np.average(time[:,1])/std
# time2 = np.average(time[:,2])/std
# time3 = np.average(time[:,3])/std
# time4 = np.average(time[:,4])/std
# time1 = np.average(time[:,1]/time[:,0])
# time2 = np.average(time[:,2]/time[:,0])
# time3 = np.average(time[:,3]/time[:,0])
# time4 = np.average(time[:,4]/time[:,0])
# print(std)
# print(rrtt)

# upperlabels = [ 1.0, str(round(time1, 2))+'X', str(round(time2,2))+'X', str(round(time3,2))+'X', str(round(time4,2))+'X']

# for tick, label in zip(range(6), axes[0,0].get_xticklabels()):
    # axes[0,0].text(tick+1, 120, upperlabels[tick], horizontalalignment='center', fontsize=10, weight='semibold');
# axes[1,1].text(3, 95, 'incomplete', horizontalalignment='center', fontsize=9);

# plt.savefig(figname1,bbox_inches = 'tight', dpi=1000)
# plt.savefig(figname2,bbox_inches = 'tight', dpi=500)
# plt.show()
