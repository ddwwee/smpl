#ifndef WALKER_SCHEDULING_POLICIES_H
#define WALKER_SCHEDULING_POLICIES_H

#include <boost/functional/hash.hpp>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <stdlib.h>
#include <utility>
#include <vector>
#include <smpl/types.h>

class SchedulingPolicy {
public:
    SchedulingPolicy(int num_queues) : m_num_queues(num_queues){};

    virtual double getActionSpaceProb(int state_id, int hidx) = 0;

    virtual int getAction() = 0;

    int numQueues() const { return m_num_queues; }

private:
    int m_num_queues;
};

class UniformlyRandomPolicy : public SchedulingPolicy {
public:
    UniformlyRandomPolicy(int _num_queues, unsigned int _seed)
        : SchedulingPolicy(_num_queues), m_seed{_seed} {
        srand(_seed);
    }

    inline virtual double getActionSpaceProb(int state_id, int hidx) {
        return 0.5;
    }

    int getAction() override { throw "Not Implemented"; }

private:
    unsigned int m_seed;
};

class RoundRobinPolicy : public SchedulingPolicy {
public:
    RoundRobinPolicy(int num_queues) : SchedulingPolicy(num_queues) {}
    inline virtual double getActionSpaceProb(int state_id, int hidx) {
        if ((hidx - 1) == m_queue) {
            if (hidx == numQueues()) m_queue = (m_queue + 1) % numQueues();
            return 1.0;
        } else {
            if (hidx == numQueues()) m_queue = (m_queue + 1) % numQueues();
            return 0.0;
        }
    }

    int getAction() override { throw "Not Implemented"; }

private:
    int m_queue = 0;
    int m_iter = 0;
};

class MABPolicy : public SchedulingPolicy {
public:
    MABPolicy(int num_arms) : SchedulingPolicy(num_arms) {}

    int numArms() { return this->numQueues(); }

    double getActionSpaceProb(int state_id, int hidx) override {
        throw "Not Implemented";
    }

    int getAction() = 0;

    virtual void updatePolicy(int arm) = 0;
};

class DTSPolicy : public MABPolicy {
public:
    DTSPolicy(int num_arms, unsigned int seed);
    ~DTSPolicy();

    int getAction() override;
    void updatePolicy(int arm);
    void currBestH(int newH, int idx);
    void progress(int idx);
    void resetReward();

private:
    unsigned int m_seed;
    int m_num_queues;
    int reward;
    std::vector<double> m_alphas{}, m_betas{};
    std::vector<int> vBestH, vPrevBestH;
    double m_C = 10;
    const gsl_rng_type *m_gsl_rand_T;
    gsl_rng *m_gsl_rand;
};

#endif
