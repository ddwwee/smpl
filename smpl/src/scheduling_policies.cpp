#include <limits>
#include <smpl/scheduling_policies.h>

DTSPolicy::DTSPolicy(int _num_arms, unsigned int _seed)
    : MABPolicy(_num_arms), m_seed{_seed} {
    m_alphas.resize(_num_arms, 1);
    m_betas.resize(_num_arms, 1);
    vBestH.resize(_num_arms,std::numeric_limits<int>::max());
    vPrevBestH.resize(_num_arms,std::numeric_limits<int>::max() );

    reward = 0;
    srand(_seed);
    gsl_rng_env_setup();
    m_gsl_rand_T = gsl_rng_default;
    m_gsl_rand = gsl_rng_alloc(m_gsl_rand_T);
}

DTSPolicy::~DTSPolicy() { gsl_rng_free(m_gsl_rand); }

int DTSPolicy::getAction() {
    std::vector<double> rep_likelihoods(this->numArms(), 0);
    double best_likelihood = -1;
    for (int i = 0; i < this->numArms(); i++) {
        rep_likelihoods[i] = gsl_ran_beta(m_gsl_rand, m_alphas[i], m_betas[i]);
        if (rep_likelihoods[i] > best_likelihood)
            best_likelihood = rep_likelihoods[i];
    }

    // because of quantization we can get the exact same random value
    // for multiple queues more often than we'd like
    // especially when beta is very low (we get 1 very easily)
    // or when alpha is very low (we get 0 very easily)
    // in these cases, there is a bias toward the lower index queues
    // because they "improve" best_rand first
    // so when there are multiple queues near the best_rand value,
    // we will choose uniformly at random from them
    std::vector<int> near_best_likelihood;
    for (int i = 0; i < this->numArms(); i++) {
        if (fabs(best_likelihood - rep_likelihoods[i]) < 0.0001) {
            near_best_likelihood.push_back(i);
        }
    }

    int best_id = near_best_likelihood[rand() % near_best_likelihood.size()];
    return best_id;
}

void DTSPolicy::resetReward(){
    reward = 0;
}
void DTSPolicy::updatePolicy(int _arm) {
    if (reward > 0)
        m_alphas[_arm] += 1;
    else
        m_betas[_arm] += 1;
    if (m_alphas[_arm] + m_betas[_arm] > m_C) {
        m_alphas[_arm] *= (m_C / (m_C + 1));
        m_betas[_arm] *= (m_C / (m_C + 1));
    }
    reward = 0;
}
void DTSPolicy::progress(int idx) {
    if (vBestH[idx] < vPrevBestH[idx]) {
        vPrevBestH[idx] = vBestH[idx];
        ++reward;
    }
}

void DTSPolicy::currBestH(int newH, int idx) {
    if (newH < vPrevBestH[idx]){ 
        vBestH[idx] = newH;
    }
}

