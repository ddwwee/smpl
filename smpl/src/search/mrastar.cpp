////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018, Andrew Dornbush
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     1. Redistributions of source code must retain the above copyright notice
//        this list of conditions and the following disclaimer.
//     2. Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//     3. Neither the name of the copyright holder nor the names of its
//        contributors may be used to endorse or promote products derived from
//        this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
////////////////////////////////////////////////////////////////////////////////

/// \author Andrew Dornbush

#include <smpl/search/mrastar.h>

#include <algorithm>
#include <assert.h>
#include <stdlib.h>
#include <time.h>

#include <sbpl/discrete_space_information/environment.h>
#include <sbpl/utils/key.h>

#include <smpl/console/console.h>
#include <smpl/graph/manip_lattice.h>
#include <smpl/time.h>

namespace smpl {

static const char *LOG = "search.mrastar";
static const char *ELOG = "search.mrastar.expansions";
static const char *SLOG = "search.mrastar.successors";

static double GetTime() { return to_seconds(clock::now().time_since_epoch()); }

MRAStar::MRAStar(DiscreteSpaceInformation *space, Heuristic *heuristic)
    : SBPLPlanner(), m_heur(heuristic) {
    if (!space || !heuristic) {
        SMPL_ERROR("MRAStar planner initialization failed");
    }
    environment_ = space;

    // Overwrite default members for ReplanParams to represent a single optimal
    // search
    m_params.initial_eps = 1.0;
    m_params.final_eps = 1.0;
    m_params.dec_eps = 0.2; // NOTE: same initial epsilon delta as ARA*
    m_params.return_first_solution = false;
    m_params.max_time = 0.0;
    m_params.repair_time = 0.0;

    m_open =
        new OpenList[NUM_RES_LEVEL + 1]; // anchor, high_res, mid_res, low_res

    dts_agent = new DTSPolicy(NUM_RES_LEVEL, 101);
}

MRAStar::~MRAStar() {
    clear();
    if (m_open != NULL) delete[] m_open;
}

int MRAStar::set_start(int start_state) {
    m_start_state = get_state(start_state);
    if (!m_start_state) {
        return 0;
    }
    return 1;
}

int MRAStar::set_goal(int goal_state) {
    m_goal_state = get_state(goal_state);
    if (!m_goal_state) {
        return 0;
    }
    // SMPL_INFO("goal res: %d", m_goal_state->res);
    return 1;
}

int MRAStar::replan(double allowed_time, std::vector<int> *solution) {
    int solcost;
    return replan(allowed_time, solution, &solcost);
}

int MRAStar::replan(double allowed_time, std::vector<int> *solution,
                    int *solcost) {
    ReplanParams params = m_params;
    params.max_time = allowed_time;
    return replan(solution, params, solcost);
}

int MRAStar::replan(std::vector<int> *solution, ReplanParams params) {
    int solcost;
    return replan(solution, params, &solcost);
}

int MRAStar::replan(std::vector<int> *solution, ReplanParams params,
                    int *solcost) {
    if (!check_params(params)) { // errors printed within
        return 0;
    }

    m_params = params;

    SMPL_INFO_NAMED(LOG, "Generic Search parameters:");
    SMPL_INFO_NAMED(LOG, "  Initial Epsilon: %0.3f", m_params.initial_eps);
    SMPL_INFO_NAMED(LOG, "  Final Epsilon: %0.3f", m_params.final_eps);
    SMPL_INFO_NAMED(LOG, "  Delta Epsilon: %0.3f", m_params.dec_eps);
    SMPL_INFO_NAMED(LOG, "  Return First Solution: %s",
                    m_params.return_first_solution ? "true" : "false");
    SMPL_INFO_NAMED(LOG, "  Max Time: %0.3f", m_params.max_time);
    SMPL_INFO_NAMED(LOG, "  Repair Time: %0.3f", m_params.repair_time);
    SMPL_INFO_NAMED(LOG, "MRA Search parameters:");
    SMPL_INFO_NAMED(LOG, "  MRA Epsilon: %0.3f", m_initial_eps_mra);
    SMPL_INFO_NAMED(LOG, "  Max Expansions: %d", m_max_expansions);

    // environment_->EnsureHeuristicsUpdated(true); // TODO: support backwards
    // search

    // TODO: pick up from where last search left off and detect lazy
    // reinitializations
    reinit_search();

    m_eps = m_params.initial_eps;
    m_eps_mra = m_initial_eps_mra;
    // m_eps_mra = 10.0;
    m_eps_satisfied = (double)INFINITECOST;

    // reset time limits
    m_num_expansions = 0;
    m_elapsed = 0.0;

    double start_time = GetTime();

    ++m_call_number;
    reinit_state(m_goal_state);
    reinit_state(m_start_state);
    m_start_state->g = 0;

    // insert start state into all heaps with key(start, i) as priority
    compute_key(m_start_state);
    m_open[0].push(&m_start_state->multiState[0]);
    SMPL_DEBUG_NAMED(LOG,
                     "Inserted start state %d into anchor search with f = %d",
                     m_start_state->state_id, m_start_state->multiState[0].f);
    for (int i = 1; i <= m_start_state->res; ++i) {
        m_open[i].push(&m_start_state->multiState[i]);
        SMPL_DEBUG_NAMED(
            LOG, "Inserted start state %d into search %d with f = %d",
            m_start_state->state_id, i, m_start_state->multiState[0].f);
    }

    double end_time = GetTime();
    m_elapsed += (end_time - start_time);

    while (!m_open[0].empty() && !time_limit_reached()) {
        start_time = GetTime();

        int hidx = dts_agent->getAction() + 1;

        // for (int hidx = 1; hidx <= NUM_RES_LEVEL; ++hidx) {
        // if (m_open[0].empty()) break;

        // if(m_rep_ids[hidx] != rep_id) continue;


        if (!m_open[hidx].empty() &&
            get_minf(m_open[hidx]) <=
                m_eps_mra * get_minf(m_open[0])) // constraint satisfied
        {
            if (m_goal_state->g <= get_minf(m_open[hidx])) {
                m_eps_satisfied = m_eps * m_eps_mra;
                extract_path(solution, solcost);
                return 1;
            } else {
                MRAState *s = state_from_open_state(m_open[hidx].min());
                expand(s, hidx);
                dts_agent->progress(hidx - 1);
            }
        } else { // constraint not satisfied, expand from anchor
            if (m_goal_state->g <= get_minf(m_open[0])) {
                m_eps_satisfied = m_eps * m_eps_mra;
                extract_path(solution, solcost);
                return 1;
            } else {
                MRAState *s = state_from_open_state(m_open[0].min());
                expand(s, 0);
            }
        }
        //}
        dts_agent->updatePolicy(hidx - 1);
        end_time = GetTime();
        m_elapsed += (end_time - start_time);
    }

    if (m_open[0].empty()) {
        SMPL_DEBUG_NAMED(LOG, "Anchor search exhausted");
        SMPL_INFO("Anchor search exhausted");
    }
    if (time_limit_reached()) {
        SMPL_DEBUG_NAMED(LOG, "Time limit reached");
        SMPL_INFO("Time limit reached");
    }

    return 0;
}

int MRAStar::force_planning_from_scratch() { return 0; }

int MRAStar::force_planning_from_scratch_and_free_memory() { return 0; }

void MRAStar::costs_changed(const StateChangeQuery &changes) {}

void MRAStar::costs_changed() {}

int MRAStar::set_search_mode(bool bSearchUntilFirstSolution) {
    return m_params.return_first_solution = bSearchUntilFirstSolution;
}

void MRAStar::set_initialsolution_eps(double eps) {
    m_params.initial_eps = eps;
}

double MRAStar::get_initial_eps() { return m_params.initial_eps; }

double MRAStar::get_solution_eps() const { return m_eps_satisfied; }

double MRAStar::get_final_epsilon() { return m_eps_satisfied; }

double MRAStar::get_final_eps_planning_time() { return m_elapsed; }

double MRAStar::get_initial_eps_planning_time() { return m_elapsed; }

int MRAStar::get_n_expands() const { return m_num_expansions; }

int MRAStar::get_n_expands_init_solution() { return m_num_expansions; }

void MRAStar::get_search_stats(std::vector<PlannerStats> *s) {}

void MRAStar::set_initial_mra_eps(double eps) { m_initial_eps_mra = eps; }

void MRAStar::set_final_eps(double eps) { m_params.final_eps = eps; }

void MRAStar::set_dec_eps(double eps) { m_params.dec_eps = eps; }

void MRAStar::set_max_expansions(int expansion_count) {
    m_max_expansions = expansion_count;
}

void MRAStar::set_max_time(double max_time) { m_params.max_time = max_time; }

double MRAStar::get_initial_mra_eps() const { return m_initial_eps_mra; }

double MRAStar::get_final_eps() const { return m_params.final_eps; }

double MRAStar::get_dec_eps() const { return m_params.dec_eps; }

int MRAStar::get_max_expansions() const { return m_max_expansions; }

double MRAStar::get_max_time() const { return m_params.max_time; }

bool MRAStar::check_params(const ReplanParams &params) {
    if (params.initial_eps < 1.0) {
        SMPL_ERROR("Initial Epsilon must be greater than or equal to 1");
        return false;
    }

    if (params.final_eps > params.initial_eps) {
        SMPL_ERROR(
            "Final Epsilon must be less than or equal to initial epsilon");
        return false;
    }

    if (params.dec_eps <= 0.0) {
        SMPL_ERROR("Delta epsilon must be strictly positive");
        return false;
    }

    if (m_initial_eps_mra < 1.0) {
        SMPL_ERROR("MRA Epsilon must be greater than or equal to 1");
        return false;
    }

    if (params.return_first_solution && params.max_time <= 0.0 &&
        m_max_expansions <= 0) {
        SMPL_ERROR("Max Time or Max Expansions must be positive");
        return false;
    }

    return true;
}

bool MRAStar::time_limit_reached() const {
    if (m_params.return_first_solution) {
        return false;
    } else if (m_params.max_time > 0.0 && m_elapsed >= m_params.max_time) {
        return true;
    } else if (m_max_expansions > 0 && m_num_expansions >= m_max_expansions) {
        return true;
    } else {
        return false;
    }
}

MRAState *MRAStar::get_state(int state_id) {
    assert(state_id >= 0 &&
           state_id < environment_->StateID2IndexMapping.size());
    int *idxs = environment_->StateID2IndexMapping[state_id];
    if (idxs[MHAMDP_STATEID2IND] == -1) {
        // overallocate search state for appropriate heuristic information
        size_t state_size =
            sizeof(MRAState) + sizeof(MRAState::HeapData) * (NUM_RES_LEVEL + 1);
        MRAState *s = (MRAState *)malloc(state_size);

        new (s) MRAState;
        for (int i = 0; i < NUM_RES_LEVEL; ++i) {
            new (&s->multiState[1 + i]) MRAState::HeapData;
        }

        size_t mra_state_idx = m_search_states.size();
        init_state(s, mra_state_idx, state_id);

        /*
         *if (s->res > 3) {
         *    SMPL_ERROR("New created state with res > 3");
         *}
         */

        // map graph state to search state
        idxs[MHAMDP_STATEID2IND] = mra_state_idx;
        m_search_states.push_back(s);

        return s;
    } else {
        int ssidx = idxs[MHAMDP_STATEID2IND];
        return m_search_states[ssidx];
        /*
         *if (s->res > 3) {
         *    SMPL_ERROR("Existing state with res > 3");
         *}
         */
        // return s;
    }
}

void MRAStar::clear() {
    clear_open_lists();

    // free states
    for (size_t i = 0; i < m_search_states.size(); ++i) {
        // unmap graph to search state
        int state_id = m_search_states[i]->state_id;
        int *idxs = environment_->StateID2IndexMapping[state_id];
        idxs[MHAMDP_STATEID2IND] = -1;

        // free search state
        free(m_search_states[i]);
    }

    // empty state table
    m_search_states.clear();

    m_start_state = NULL;
    m_goal_state = NULL;
}

void MRAStar::init_state(MRAState *state, size_t mra_state_idx, int state_id) {
    state->call_number = 0; // not initialized for any iteration
    state->state_id = state_id;
    state->h = m_heur->GetGoalHeuristic(state->state_id);
    state->res = ((ManipLattice *)environment_)->GetResLevel(state->state_id);
    // SMPL_INFO("init state to res: %d", state->res);
    /*
     *if (state->res > 3) {
     *    SMPL_ERROR("New created state with res > 3 (env)");
     *}
     */

    for (int i = 0; i <= NUM_RES_LEVEL; ++i) {
        state->closed_in[i] = false;
        state->multiState[i].me = state;
        state->multiState[i].f = INFINITECOST;
    }
}

void MRAStar::reinit_state(MRAState *state) {
    if (state->call_number != m_call_number) {
        state->call_number = m_call_number;
        state->g = INFINITECOST;
        state->h = m_heur->GetGoalHeuristic(state->state_id);
        state->bp = NULL;
        state->res =
            ((ManipLattice *)environment_)->GetResLevel(state->state_id);
        assert(state->res <= 3);
        // SMPL_INFO("init state to res: %d", state->res);
        for (int i = 0; i <= NUM_RES_LEVEL; ++i) {
            state->closed_in[i] = false;
            state->multiState[i].f = INFINITECOST;
        }
    }
}

void MRAStar::reinit_search() { clear_open_lists(); }

void MRAStar::clear_open_lists() {
    for (int i = 0; i <= NUM_RES_LEVEL; ++i) {
        m_open[i].clear();
    }
}

void MRAStar::compute_key(MRAState *state) {
    // anchor is A*
    state->multiState[0].f = state->g + state->h;
    // int key(state->g + m_eps * state->h);
    for (int i = 1; i <= NUM_RES_LEVEL; ++i) {
        // state->multiState[i].f = state->g + (m_eps-(i-1)*10) * state->h;
        state->multiState[i].f = state->g + m_eps * state->h;
    }
}

void MRAStar::expand(MRAState *state, int hidx) {
    // SMPL_DEBUG_NAMED(LOG, "Expanding state %d in search %d", state->state_id,
    // hidx);

    assert(!closed_in_anc_search(state));

    state->closed_in[hidx] = true; // remove s from corresponding list
    if (m_open[hidx].contains(&state->multiState[hidx]))
        m_open[hidx].erase(&state->multiState[hidx]);
    ++m_num_expansions;

    MultiRes::Res_Level resLevel;
    switch (hidx) {
    case 0:
        resLevel = MultiRes::Invalid;      // special case of Anchor, see if full action expansion would help
        break;
    case 1:
        resLevel = MultiRes::HIGH_RES;
        break;
    case 2:
        resLevel = MultiRes::MID_RES;
        break;
    case 3:
        resLevel = MultiRes::LOW_RES;
        break;
    default:
        SMPL_ERROR("Expand: this hidx: %d is not supposed to exist", hidx);
    }
    std::vector<int> succ_ids;
    std::vector<int> costs;
    ((ManipLattice *)environment_)
        ->GetSuccs(state->state_id, resLevel, &succ_ids, &costs);
    assert(succ_ids.size() == costs.size());

    for (size_t sidx = 0; sidx < succ_ids.size(); ++sidx) {
        int cost = costs[sidx];
        MRAState *succ_state = get_state(succ_ids[sidx]);
        reinit_state(succ_state);

        /*
         *if (succ_state->res > 3) {
         *    SMPL_ERROR("update res: %d", succ_state->res);
         *}
         */
        // SMPL_DEBUG_NAMED(LOG, " Successor %d", succ_state->state_id);

        int new_g = state->g + costs[sidx];
        if (new_g < succ_state->g) {
            succ_state->g = new_g;
            succ_state->bp = state;
            //if (!closed_in_anc_search(succ_state)) {
                compute_key(succ_state); // compute key for all multiState
                insert_or_update(succ_state, 0);
                // SMPL_DEBUG_NAMED(LOG, "  Update in search %d with f = %d", 0,
                // succ_state->multiState[0].f);
                // int fn = succ_state->multiState[0].f;               // all f
                // values are equal

                for (int i = 1; i <= succ_state->res; ++i) {
                    if (!closed_in_add_search(succ_state, i)) {
                        insert_or_update(succ_state, i);
                        dts_agent->currBestH(succ_state->h, i - 1);
                        // SMPL_DEBUG_NAMED(LOG,
                        //"  Update in search %d with f = %d", i,
                        // succ_state->multiState[i].f);
                    }
                }
            //}
        }
    }
}

MRAState *MRAStar::state_from_open_state(MRAState::HeapData *open_state) {
    return open_state->me;
}

int MRAStar::get_minf(OpenList &pq) const { return pq.min()->f; }

void MRAStar::insert_or_update(MRAState *state, int hidx) {
    if (m_open[hidx].contains(&state->multiState[hidx])) {
        m_open[hidx].update(&state->multiState[hidx]);
    } else {
        m_open[hidx].push(&state->multiState[hidx]);
    }
}

void MRAStar::extract_path(std::vector<int> *solution_path, int *solcost) {
    SMPL_DEBUG_NAMED(LOG, "Extracting path");
    solution_path->clear();
    *solcost = 0;
    for (MRAState *state = m_goal_state; state; state = state->bp) {
        solution_path->push_back(state->state_id);
        if (state->bp) {
            *solcost += (state->g - state->bp->g);
        }
    }

    // TODO: special cases for backward search
    std::reverse(solution_path->begin(), solution_path->end());
}

bool MRAStar::closed_in_anc_search(MRAState *state) const {
    return state->closed_in[0];
}

bool MRAStar::closed_in_add_search(MRAState *state, int hidx) const {
    return state->closed_in[hidx];
}

} // namespace smpl
